const SMTPServer = require("smtp-server").SMTPServer;

const options = {
  secure: false,
  name: "node_smtp_server",
  banner: "Welcome to custom smtp-server",
  authOptional: true,
  allowInsecureAuth: true
};

const server = new SMTPServer({
  options,
  onConnect(session, callback) {
    console.log("Connected:", session);
    return callback();
  },
  onClose(session) {
    console.log("Closing Connection", session);
  },
  onMailFrom(address, session, callback) {
    console.log("mailFrom", address, session);
    return callback();
  },
  onRcptTo(address, session, callback) {
    console.log("rcptFrom", address, session);
    return callback();
  },
  onData(stream, session, callback) {
    stream.pipe(process.stdout);
    stream.on("end", callback);
  }
});

server.on("error", err => {
  console.log("Error %s", err.message);
});

server.listen(3030, () => {
  console.log("STMP Listening on:3030");
});

// refer: http://nodemailer.com/extras/smtp-server/